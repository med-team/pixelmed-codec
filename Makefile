all:	pixelmed_codec.jar pixelmed_imageio.jar

TAR = gnutar
#TAR = tar
COMPRESS = bzip2
COMPRESSEXT = bz2

SUBDIRS = \
	com/pixelmed/codec/jpeg \
	com/pixelmed/imageio
	
SUBPACKAGES = \
	com.pixelmed.codec.jpeg \
	com.pixelmed.imageio

ADDITIONALFILES = \
	COPYRIGHT \
	Makefile \
	Makefile.common.mk \
	README

ADDITIONALSOURCEFILES = \
	Doxyfile

BINARYRELEASEFILES = \
	pixelmed_codec.jar \
	pixelmed_imageio.jar \
	BUILDDATE \
	${ADDITIONALFILES}

SOURCERELEASEFILES = \
	${ADDITIONALFILES} \
	${ADDITIONALSOURCEFILES} \
	${SUBDIRS}

DEPENDENCYRELEASEFILESWITHOUTREADME = \
	lib/junit/junit-4.8.1.jar \
	LICENSES

DEPENDENCYRELEASEFILES = \
	${DEPENDENCYRELEASEFILESWITHOUTREADME} \
	README

JAVADOCRELEASEFILES = \
	${JAVADOCFILES}

DOXYGENRELEASEFILES = \
	${DOXYGENFILES}

JAVADOCFILES = \
	docs/javadoc

DOXYGENFILES = \
        docs/doxygen

#OTHERDOCRELEASEFILES = \
#

PATHTOROOT = .

include ${PATHTOROOT}/Makefile.common.mk

metainf:
	mkdir -p META-INF/services
	rm -f META-INF/services/javax.imageio.spi.ImageReaderSpi
	echo >META-INF/services/javax.imageio.spi.ImageReaderSpi "com.pixelmed.imageio.JPEGLosslessImageReaderSpi"

pixelmed_codec.jar:
	(cd com/pixelmed/codec/jpeg; make all)
	date >BUILDDATE
	jar -cvf $@ BUILDDATE COPYRIGHT \
		com/pixelmed/codec/jpeg/*.class

pixelmed_imageio.jar:	metainf
	(cd com/pixelmed/codec/jpeg; make all)
	(cd com/pixelmed/imageio; make all)
	date >BUILDDATE
	jar -cvf $@ BUILDDATE COPYRIGHT \
		META-INF/services/javax.imageio.spi.ImageReaderSpi \
		com/pixelmed/codec/jpeg/*.class \
		com/pixelmed/imageio/*.class \

changelog:
	rm -f CHANGES
	cvsps -u -q | egrep -v '^(PatchSet|Author:|Branch:|Tag:|Members:|Log:)' | fgrep -v '*** empty log message ***' | grep -v '^[ ]*$$' | sed -e 's/:[0-9.]*->[0-9.]*//' -e 's/:INITIAL->[0-9.]*//' -e 's/^Date: \([0-9][0-9][0-9][0-9]\/[0-9][0-9]\/[0-9][0-9]\) [0-9:]*$$/\1/' >CHANGES
	bzip2 <CHANGES >CHANGES.bz2
	
clean:	cleanallexceptjar
	rm -f pixelmed_codec.jar pixelmed_imageio.jar

cleanallexceptjar:	cleansubdirs
	rm -f *~ *.class .exclude.list
	rm -rf META-INF

cleansubdirs:
	for d in ${SUBDIRS}; \
	do \
		if [ -d $$d ]; then \
			(cd $$d; echo "Cleaning in $$d"; make clean); \
		fi; \
	done

archivesource: clean .exclude.list
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - Makefile ${ADDITIONALFILES} ${SUBDIRS} | ${COMPRESS} > pixelmedjavacodec_source_archive.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

archivejavadoc: .exclude.list #javadoc
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - ${JAVADOCFILES} | ${COMPRESS} > pixelmedjavacodec_javadoc_archive.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

archivedoxygen: .exclude.list #doxygen
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - ${DOXYGENFILES} | ${COMPRESS} > pixelmedjavacodec_javadoc_archive.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

#releaseall:	changelog sourcerelease javadocrelease doxygenrelease binaryrelease dependencyrelease otherdocrelease
releaseall:	changelog sourcerelease javadocrelease doxygenrelease binaryrelease dependencyrelease

binaryrelease: cleanallexceptjar .exclude.list pixelmed_codec.jar pixelmed_imageio.jar #javadoc doxygen
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - ${BINARYRELEASEFILES} | ${COMPRESS} > pixelmedjavacodec_binaryrelease.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

#otherdocrelease:
#	export COPYFILE_DISABLE=true; \
#	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
#	${TAR} -cv -f - ${OTHERDOCRELEASEFILES} | ${COMPRESS} > pixelmedjavacodec_otherdocsrelease.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

dependencyrelease:	.exclude.list
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - ${DEPENDENCYRELEASEFILES} | ${COMPRESS} > pixelmedjavacodec_dependencyrelease.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

sourcerelease: clean .exclude.list #clean javadoc doxygen
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - ${SOURCERELEASEFILES} | ${COMPRESS} > pixelmedjavacodec_sourcerelease.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

javadocrelease:  .exclude.list #clean javadoc doxygen
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - ${JAVADOCRELEASEFILES} | ${COMPRESS} > pixelmedjavacodec_javadocrelease.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

doxygenrelease: .exclude.list #clean javadoc doxygen
	export COPYFILE_DISABLE=true; \
	export COPY_EXTENDED_ATTRIBUTES_DISABLE=true; \
	${TAR} -cv -X .exclude.list -f - ${DOXYGENRELEASEFILES} | ${COMPRESS} > pixelmedjavacodec_doxygenrelease.`date '+%Y%m%d'`.tar.${COMPRESSEXT}

.exclude.list:	Makefile
	echo "Making .exclude.list"
	echo ".DS_Store" > $@
	echo ".directory" >> $@
	echo "*.tar.gz" >> $@
	echo "*.tar.bz2" >> $@
	echo "*.tar.bz2" >> $@
	#ls *.jar | grep -v 'pixelmed_codec.jar' >> $@
	echo "cleanerdst.*" >> $@
	echo "cleanersrc.*" >> $@
	#find . -path  '*/cleanerdst.*' | sed 's/[.][/]//' >>$@
	#find . -path  '*/cleanersrc.*' | sed 's/[.][/]//' >>$@
	find . -path '*/NOTES*' | sed 's/[.][/]//' >>$@
	find . -path '*/CVS*' | sed 's/[.][/]//' >>$@
	echo "com/pixelmed/web/favicon.ill" >> $@
	#cat $@

# used to link to "http://www.junit.org/apidocs/" but this no longer works ... use version-specific "http://javasourcecode.org/html/open-source/junit/junit-4.8.1/" instead
javadoc:
	rm -rf docs/javadoc
	javadoc \
		-classpath .:lib/additional/excalibur-bzip2-1.0.jar:lib/additional/hsqldb.jar:lib/additional/vecmath1.2-1.14.jar:lib/additional/commons-codec-1.3.jar:lib/additional/commons-net-ftp-2.0.jar:lib/additional/jmdns.jar:lib/additional/jpedalSTD.jar:lib/junit/junit-4.8.1.jar \
		-link http://download.oracle.com/javase/1.5.0/docs/api/ \
		-link http://jpedal.org/javadoc/ \
		-link http://www.hsqldb.org/doc/src/ \
		-link http://javasourcecode.org/html/open-source/junit/junit-4.8.1/ \
		-protected -d docs/javadoc \
		-encoding "UTF8" \
		${SUBPACKAGES}

doxygen:
	rm -rf docs/doxygen
	doxygen Doxyfile

installinpixelmed:	pixelmed_codec.jar pixelmed_imageio.jar
	cp pixelmed_codec.jar ../pixelmed/imgbook/lib/additional/
	cp pixelmed_imageio.jar ../pixelmed/imgbook/lib/additional/

