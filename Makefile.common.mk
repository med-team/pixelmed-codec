#
# Note that PATHTOROOT must have been specified prior to including this file
#

JUNITJAR = ${PATHTOROOT}/lib/junit/junit-4.8.1.jar

PATHTOTESTFILESFROMROOT = ./testpaths

PATHTOTESTRESULTSFROMROOT = ./testresults

JAVAVERSIONTARGET=1.7

JAVACTARGETOPTIONS=-target ${JAVAVERSIONTARGET} -source ${JAVAVERSIONTARGET} -bootclasspath $${JAVAVERSIONTARGETJARFILE}

.SUFFIXES:	.class .java .ico .png

JAVACOPTIONS = -O ${JAVACTARGETOPTIONS} -encoding "UTF8" -Xlint:deprecation

.java.class:
	export JAVAVERSIONTARGETJARFILE=`/usr/libexec/java_home -v ${JAVAVERSIONTARGET} | tail -1`/jre/lib/rt.jar; javac ${JAVACOPTIONS} \
		-classpath ${PATHTOROOT} \
		-sourcepath ${PATHTOROOT} $<

clean:
	rm -f *~ *.class core *.bak
